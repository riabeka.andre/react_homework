import React, {Component} from 'react';
import './App.css';
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import MainComponent from "./components/MainComponent";
import LikedContainer from "./components/LikedItems/LikedContainer";
import Nav from './services/Nav'
import CartContainer from "./components/Cart/CartContainer";

class App extends Component {

  render() {
    return (
        <Router>
          <Nav/>
          <Switch>
            <Route path="/" exact component={MainComponent}/>
            <Route path="/likedContainer" component={LikedContainer}/>
            <Route path="/cartContainer" component={CartContainer}/>
          </Switch>
        </Router>
    )
  }
}

export default App


