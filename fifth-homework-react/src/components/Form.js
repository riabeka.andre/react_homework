import React from "react";
import {connect} from "react-redux";
import CartItem from "./Cart/CartItem";
class Form extends React.Component {
    constructor(props){
        super(props)
    }
    submit (event){
        event.preventDefault();
        let inputs = [this.nameInput.value, this.secondNameInput.value, this.phoneNumberInput.value, this.ageInput.value, this.addressInput.value];
        function exact(element, index, array){
         return element !== ''
        }
        try {
            if(inputs.every(exact) && this.props.cart.length !== 0 && this.phoneNumberInput.value.length >= 10 && (this.ageInput.value >= 12 && this.ageInput.value <=120)){
                this.props.onSubmit(this.nameInput.value, this.secondNameInput.value, this.ageInput.value, this.phoneNumberInput.value, this.addressInput.value);
                console.log('Dear', this.nameInput.value, 'You have just ordered', this.props.cart.length, 'goods',
                    this.props.cart.map((item) => item.name),
                    'Your number is', this.phoneNumberInput.value,
                    'Your address is', this.addressInput.value
                );
                this.props.onCleanCart();
            }
        }
        catch (e) {
            console.log('some', e, 'happened')
        }
    }
    render(){
        return(
            <form className='cart-form'>
                <label className='label' htmlFor='name'>Your First Name</label><br/>
                <input className='cart-input' type='text' id='name' ref={(input) => this.nameInput = input}/><br/>
                <label className='label' htmlFor='secondName'>Your Second Name</label><br/>
                <input className='cart-input' type='text' id='secondName' ref={(input) => this.secondNameInput = input}/><br/>
                <label className='label' htmlFor='age'>Your age</label><br/>
                <input className='cart-input' type='number' id='age' ref={(input) => this.ageInput = input}/><br/>
                <label className='label' htmlFor='phone'>Phone number</label><br/>
                <input className='cart-input' type='number' id='phone' ref={(input) => this.phoneNumberInput = input}/><br/>
                <label className='label' htmlFor='address'>Your address</label><br/>
                <textarea className='textarea' id='address' ref={(input) => this.addressInput = input}/><br/>
                <button className='button-checkout' onClick={this.submit.bind(this)}>Checkout</button>
            </form>
        )
    }
}

export default connect(
    state => ({
        name: state.name,
        cart: state.cart
    }),
    dispatch => ({
            onSubmit: (name, secondName, age, phoneNumber, address) => {
                const payload = {
                    name,
                    secondName,
                    age,
                    phoneNumber,
                    address
                };
                dispatch({type: 'NAME', payload})
            },
            onCleanCart: () => {
                dispatch({type: 'CLEAN'})
            },
        })
)(Form);