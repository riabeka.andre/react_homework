import React from "react";
import {connect} from "react-redux";

class CartItem extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            modalIsOpen: false,
            liked: 'disactive'
        }
    }
    modal (){
        this.setState(
            {
                modalIsOpen: true
            }
        );
        this.props.onModalOpen();
    }
    doNotRemove(){
        this.setState(
            {
                modalIsOpen: false
            }
        );
        this.props.onModalOpen();
    }
    ConfirmRemoveFromCart (){
        this.props.onRemoveFromCart(this.props);
        this.setState({
            addedToCart: false,
            modalIsOpen: true
        });
        this.props.onModalOpen();
    };
    toggleLiked (){
        if(this.state.liked === 'disactive'){
            this.props.onAddToLiked(this.props.item);
            this.setState(
                {
                    liked: 'active',
                }
            );

        } else {
            this.props.onRemoveFromLiked(this.props);
            this.setState(
                {liked: 'disactive'}
            );
        }
    }
    componentDidMount() {
        this.props.liked.forEach(item => {
            if(item.article === this.props.article){
                this.setState({
                    liked: 'active',
                })
            }
        });
    }

    render() {
        const body = this.state.modalIsOpen && <div className='shadow-for-modal'>
            <div className='modal' >Are You sure You want to remove from cart this item?
                <button className='modalButton'
                        onClick={this.ConfirmRemoveFromCart.bind(this)}>Yes, remove from cart</button>
                <button className='modalButton' onClick={this.doNotRemove.bind(this)}>No, don't remove</button>
            </div>
        </div>;
        return(
            <>
                {body}
                <div className='item'><div className='delete' onClick={this.modal.bind(this)}>X</div>
                    <div className="items name">{this.props.name}</div>
                    <div className="items price">Price: <br/> {this.props.price} UAH</div>
                    <div className="items article">Article: {this.props.article}</div>
                    <div className="items color">Color: {this.props.color}</div>
                    <img className='img' src={this.props.src} alt="" />
                    <div className='favorite'>
                        <button className={this.state.liked} onClick={this.toggleLiked.bind(this)}></button>
                    </div>
                </div>
            </>


        )
    }

}
export default connect(
    state => ({
        cart: state.cart,
        liked: state.liked,
        modalIsOpen: state.modalIsOpen
    }),
    dispatch => ({
        onRemoveFromCart: (item) => {
            dispatch({type: 'REMOVE_FROM_CART', payload: item})
        },
        onModalOpen: () => {
            dispatch({type: 'MODAL_IS_OPEN'})
        },
        onAddToLiked: (item) => {
            dispatch({type: 'ADD_TO_LIKED', payload: item})
        },
        onRemoveFromLiked: (item) => {
            dispatch({type: 'REMOVE_FROM_LIKED', payload: item})
        }
    })
)(CartItem);