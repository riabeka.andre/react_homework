import React from "react";
import CartItem from "./CartItem"

class CartItemsList extends React.Component {
    render() {
        return (
            this.props.cart.map((item) => {
                return (
                    <CartItem key={item.article}
                              name={item.name}
                              price={item.price}
                              article={item.article}
                              color={item.color}
                              src={item.url}
                              item={item}
                    />
                )
            })
        )
    }
}
export default CartItemsList;
