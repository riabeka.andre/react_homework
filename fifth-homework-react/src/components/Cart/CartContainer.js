import React from "react";
import Form from "../Form";
import CartItemsList from "./CartItemsList";
import {connect} from "react-redux";
class CartContainer extends React.Component {
    componentDidUpdate(prevProps, prevState, snapshot) {
        localStorage.setItem("cart", JSON.stringify(this.props.cart));
        localStorage.setItem("modalIsOpen", JSON.stringify(this.props.modalIsOpen));
    }
    render() {
        return (
            <div className='wrapper-for-cart-container'>
                <Form/>
                {/*<div className='App-cart'>*/}
                    <div className='cart-list'>
                        <CartItemsList cart={this.props.cart}/>
                    </div>
                {/*</div>*/}
            </div>

        )
    }


}
export default connect(
    state => ({
        cart: state.cart,
        liked: state.liked,
        modalIsOpen: state.modalIsOpen,
    }),
)(CartContainer);