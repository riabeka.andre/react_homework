import React from "react";
import Liked from "./Liked"


class LikedList extends React.Component{
    render() {
        return (

            this.props.liked.map((item) => {
                return (
                    <Liked key={item.article}
                           name={item.name}
                           price={item.price}
                           article={item.article}
                           color={item.color}
                           src={item.url}
                           item={item}
                    />
                )
            })
        )
    }


}
export default LikedList;
