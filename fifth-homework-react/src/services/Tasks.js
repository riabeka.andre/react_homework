
class ProductService {
    static getProducts = () =>{
        return async function (dispatch) {
            let products = await fetch("./goods.json")
                .then(response => response.json())
                .then(json => json.products);
            products.map((item) =>{
                dispatch({type: 'LOAD_ITEMS', payload: item
                })
            });
        }
    };
    static getItemsToLikedFromStorage = () => {
        return async function (dispatch){
            let products = JSON.parse(localStorage.getItem("liked"));
            if(products !== null){
                products.map((item) =>{
                    dispatch({type: 'ADD_TO_LIKED_FROM_STORAGE', payload: item
                    })
                });
            } else {
                localStorage.setItem("liked", JSON.stringify([]))
            }

        }
    };
    static getItemsToCartFromStorage = () => {
        return async function (dispatch){
            let products = JSON.parse(localStorage.getItem("cart"));
            if(products !== null){
                products.map((item) =>{
                    dispatch({type: 'ADD_TO_CART_FROM_STORAGE', payload: item
                    })
                });
            } else {
                localStorage.setItem("cart", JSON.stringify([]))
            }

        }
    }
}
export default ProductService;



