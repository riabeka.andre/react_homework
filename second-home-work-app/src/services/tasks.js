export default class ProductService {
    static getProducts() {
        return fetch("./goods.json")
            .then(response => response.json())
            .then(json => json.products)
    }
}