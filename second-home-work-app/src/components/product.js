import React from "react";
class Product extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            open: 'disactive'
        };
    }
    changeClass = () => {
        if(this.state.open === 'disactive'){
            this.setState({open: 'active'});
            this.props.favorites.push(this.props.article);
            localStorage.setItem("favorites", JSON.stringify(this.props.favorites));
            console.log(this.props.article);
            console.log(this.props.favorites)
        }else{
            this.setState({open: 'disactive'});
            this.props.favorites.find((value, index) => {
                if(value === this.props.article){
                    this.props.favorites.splice(index, 1);
                    localStorage.setItem("favorites", JSON.stringify(this.props.favorites));
                }
            });
        }
    };

    componentDidMount = () => {
        const liked = JSON.parse(localStorage.getItem("favorites"));
        liked.find((value,index) => {
            if(value === this.props.article){
                console.log(this.props.article);
                this.setState({open: 'active'})
            }
        })
    };

    render() {
        return (
            <div className="item">
                <div className="items name">{this.props.name}</div>
                <div className="items price">Price: <br/> {this.props.price} UAH</div>
                <div className="items article">Article: {this.props.article}</div>
                <div className="items color">Color: {this.props.color}</div>
                <img className='img' src={this.props.src} alt="" />
                <button className='add-to-carte' onClick={this.props.modal}>Add to cart</button>
                <div className='favorite'>
                    <button className={this.state.open} onClick={this.changeClass}></button>
                </div>
            </div>
        )
    }
}
export default Product