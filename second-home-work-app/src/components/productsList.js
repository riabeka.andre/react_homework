import React from "react";
import Product from "./product";

class ProductsList extends React.Component {

    render() {
        return (
            this.props.products.map((item, index) => {
                return (
                    <Product
                        key={item.article}
                        name={item.name}
                        price={item.price}
                        article={item.article}
                        color={item.color}
                        src={item.url}
                        modal={this.props.modal}
                        favorites={this.props.favorites}
                    />
                )
            })
        )
    }
}
export default ProductsList
