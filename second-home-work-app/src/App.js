import React, {Component} from 'react';
import './App.css';
import ProductsList from "./components/productsList";
import ProductService from "./services/tasks";

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            products: [],
            isOpen: false,
            count: 0,
            favorites: []
        };
    }

    modal = () => {
        this.setState({
            isOpen: true
        });
    };
    addToCart = () => {
        this.setState({
            isOpen: false,
            count: +this.state.count + +1
        });
        let {count} = this.state;
        localStorage.setItem('count', +count+1);
        console.log(count)
    };
    doNotAdd = () => {
        this.setState({
            isOpen: false,
        })
    };

    async componentDidMount() {
        let products = await ProductService.getProducts();
        this.setState({
            products: products
        });
        const count = localStorage.getItem('count');
        this.setState({ count });
        const liked = JSON.parse(localStorage.getItem("favorites"));
        if(liked !== null){
            this.setState({ favorites: liked });
        }
    }

    render() {
        const body = this.state.isOpen && <div className='shadow-for-modal'>
            <div className='modal' >Are You sure You want to add to cart this item?
                <button className='modalButton' onClick={this.addToCart}>Yes, add to cart</button>
                <button className='modalButton' onClick={this.doNotAdd}>No, don't add</button>
            </div>
        </div>;
        const cart = <button>Cart {this.state.count}</button>;
        return (
            <div className='App'>
                {cart}
                {body}
                <div className='product-list'>
                    <ProductsList products={this.state.products}
                                  modal={this.modal}
                                  addFavorites={this.addFavorites}
                                  addClass={this.state.addClass}
                                  favorites={this.state.favorites}/>
                </div>
            </div>
        )
    }
}
export default App;
