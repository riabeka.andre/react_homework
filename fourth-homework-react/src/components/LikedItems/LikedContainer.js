import React from "react";
import LikedList from "./LikedList";
import {connect} from "react-redux";

class LikedContainer extends React.Component{
    componentDidUpdate(prevProps, prevState, snapshot) {
        localStorage.setItem("liked", JSON.stringify(this.props.liked));
        localStorage.setItem("modalIsOpen", JSON.stringify(this.props.modalIsOpen))
    }
    render() {
        return (
        <div className='App'>
            <div className='product-list'>
                <LikedList liked={this.props.liked}/>
            </div>
        </div>

    )
    }
}
export default  connect(
    state => ({
        cart: state.cart,
        liked: state.liked,
        modalIsOpen: state.modalIsOpen
    }),
    dispatch => ({
        onAddToLikedFromStore: (item) => {
            dispatch({type: 'ADD_TO_LIKED_FROM_STORAGE', payload: item})
        },
})
)(LikedContainer);