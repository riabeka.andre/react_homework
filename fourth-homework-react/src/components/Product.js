import React from "react";
import {connect} from "react-redux";

class Product extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            liked: 'disactive',
            addedToCart: false,
            modalOpen: false
        }
    }
    modal (){
        if(this.state.addedToCart === false){
            this.setState(
                {
                    modalOpen: true
                }
            );
            this.props.onModalOpen();
        }
        else return false
    }
    doNotAdd(){
        this.setState(
            {
                modalOpen: false
            }
        );
        this.props.onModalOpen();
    }
    addToCart (){
            this.props.onAddToCart(this.props.item);
            this.props.onModalOpen();
            this.setState({
                addedToCart: true,
                modalOpen: false
            })
    };
    toggleLiked (){
        if(this.state.liked === 'disactive'){
            this.props.onAddToLiked(this.props.item);
            this.setState(
                {
                    liked: 'active',
                }
            );

        } else {
            this.props.onRemoveFromLiked(this.props);
            this.setState(
                {liked: 'disactive'}
            );
        }
    }
    componentDidMount() {
        this.props.liked.forEach(item => {
            if(item.article === this.props.article){
                this.setState({
                    liked: 'active',
                })
            }
        });
        this.props.cart.forEach(item => {
            if(item.article === this.props.article){
                this.setState({
                    addedToCart: true
                })
            }
        })
    }

    render() {
        const body = this.state.modalOpen && <div className='shadow-for-modal'>
            <div className='modal' >Are You sure You want to add to cart this item?
                <button className='modalButton'
                        onClick={this.addToCart.bind(this)}>Yes, add to cart</button>
                <button className='modalButton' onClick={this.doNotAdd.bind(this)}>No, don't add</button>
            </div>
        </div>;
        return (
            <>
                {body}
                <div className="item">
                    <div className="items name">{this.props.name}</div>
                    <div className="items price">Price: <br/> {this.props.price} UAH</div>
                    <div className="items article">Article: {this.props.article}</div>
                    <div className="items color">Color: {this.props.color}</div>
                    <img className='img' src={this.props.src} alt="" />
                    <button className='add-to-carte' onClick={this.modal.bind(this)}>Add to cart</button>
                    <div className='favorite'>
                        <button className={this.state.liked} onClick={this.toggleLiked.bind(this)}></button>
                    </div>
                </div>
            </>

        )
    }
}
export default connect(
    state => ({
        cart: state.cart,
        liked: state.liked,
        modalIsOpen: state.modalIsOpen
    }),
    dispatch => ({
        onAddToCart: (item) => {
            dispatch({type: 'ADD_TO_CART', payload: item})
        },
        onAddToLiked: (item) => {
            dispatch({type: 'ADD_TO_LIKED', payload: item})
        },
        onRemoveFromLiked: (item) => {
            dispatch({type: 'REMOVE_FROM_LIKED', payload: item})
        },
        onModalOpen: () => {
            dispatch({type: 'MODAL_IS_OPEN'})
        },
    })
)(Product);