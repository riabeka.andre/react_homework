import React from 'react';
import ProductsList from "./ProductList";
import {connect} from "react-redux";

 class MainComponent extends React.Component{
componentDidUpdate(prevProps, prevState, snapshot) {
    localStorage.setItem("liked", JSON.stringify(this.props.liked))
    localStorage.setItem("cart", JSON.stringify(this.props.cart))
    localStorage.setItem("modalIsOpen", JSON.stringify(this.props.modalIsOpen))
}
     render() {
         return (
             <div className='App'>
                 <div className='product-list'>
                     <ProductsList products={this.props.products}

                     />
                 </div>
             </div>
         )
     }
}
export default connect(
    state => ({
        products: state.products,
        cart: state.cart,
        liked: state.liked,
        modalIsOpen: state.modalIsOpen
    }),

)(MainComponent);
