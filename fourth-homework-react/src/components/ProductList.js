import React from "react";
import Product from "./Product"

class ProductList extends React.Component  {
    render() {
        return (
            this.props.products.map((item) => {
                return (
                    <Product key={item.article}
                             name={item.name}
                             price={item.price}
                             article={item.article}
                             color={item.color}
                             src={item.url}
                             item={item}
                    />
                )
            })
        )
    }

}
 export default ProductList
