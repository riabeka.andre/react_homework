import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import {Provider} from 'react-redux'
import ProductService from './services/Tasks'
import {createStore, applyMiddleware} from "redux";
import thunk from "redux-thunk";



const initialState = {
    products: [],
    liked: [],
    cart: [],
    modalIsOpen: false
};

function reducer (state = initialState, action) {
    if(action.type === 'LOAD_ITEMS'){
        return {
            ...state,
            products: [
                ...state.products,
                action.payload
            ]
        }
    } else if(action.type === 'ADD_TO_LIKED'){
        return {
            ...state,
            liked: [
                ...state.liked,
                action.payload
            ]
        }
    }else if(action.type === 'ADD_TO_LIKED_FROM_STORAGE'){
        return {
            ...state,
            liked: [
                ...state.liked,
                action.payload
            ]
        }
    }
    else if(action.type === 'REMOVE_FROM_LIKED'){
        const currentState = [...state.liked];
        const idx = state.liked.findIndex(fav => fav.article === action.payload.article)
        currentState.splice(idx ,1);
        return {
            ...state,
        liked:  currentState
        }

    } else if(action.type === 'ADD_TO_CART'){
        return {
            ...state,
            cart: [
                ...state.cart,
                action.payload
            ]
        }
    }else if(action.type === 'MODAL_IS_OPEN'){
        return {
            ...state,
            modalIsOpen: !state.modalIsOpen
        }
    }
    else if(action.type === 'ADD_TO_CART_FROM_STORAGE'){
        return {
            ...state,
            cart: [
                ...state.cart,
                action.payload
            ]
        }
    }
    else if(action.type === 'REMOVE_FROM_CART'){
        const currentState = [...state.cart];
        const idx = state.cart.findIndex(fav => fav.article === action.payload.article)
        currentState.splice(idx ,1);
        return {
            ...state,
            cart:  currentState
        }
    }

    return state

}

const store = createStore(reducer, applyMiddleware(thunk));
store.dispatch(ProductService.getProducts());
store.dispatch(ProductService.getItemsToLikedFromStorage());
store.dispatch(ProductService.getItemsToCartFromStorage())

ReactDOM.render(
  <React.StrictMode>
      <Provider store={store}>
          <App />
      </Provider>

  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
