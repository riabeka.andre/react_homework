import React from "react";
import {Link} from "react-router-dom"
import {connect} from "react-redux";
class Nav extends React.Component{

    render() {
        let cart = this.props.cart.length
        return(
            <nav className='nav'>
                <ul className='nav-links'>
                    <Link to='/likedContainer'>
                        <li>Favorites</li>
                    </Link>
                    <Link to='/CartContainer'>
                        <li>Cart {cart}</li>
                    </Link>
                    <Link to='/'>
                        <li>Main Page</li>
                    </Link>
                </ul>
            </nav>
        )
    }

}
export default connect(
    state => ({
        products: state.products,
        cart: state.cart,
        liked: state.liked,
        modalIsOpen: state.modalIsOpen
    }),

)(Nav);