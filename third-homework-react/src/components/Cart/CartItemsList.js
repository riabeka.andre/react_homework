import React from "react";
import CartItem from "./CartItem"

function CartItemsList (props) {
        return (
            props.build.map((item) => {
                return (
                    <CartItem key={item.article}
                              name={item.name}
                              price={item.price}
                              article={item.article}
                              color={item.color}
                              src={item.url}
                              cart={props.cart}/>
                )
            })
        )
}
export default CartItemsList;
