import React, {useState, useEffect} from "react";
import ProductService from "../../services/Tasks";
import CartItemsList from "./CartItemsList";

function CartContainer (){
    const [products, setProducts] = useState([]);
    const [cart, setCart] = useState([]);
    const [build, setBuild] = useState([]);

    useEffect ( ( )  => {
        (async function () {
            let products = await ProductService.getProducts();
            setProducts(
                products
            );
        })()},[]);
    useEffect(() => {
        let x = [];

        let cartes = JSON.parse(localStorage.getItem("cart"));
        if(cartes === null){
            setCart( [] );
            localStorage.setItem("cart", JSON.stringify(cart));
        }
        if(cartes !== null){
            setCart( cartes);
        }
        cart.map((value) => {
            products.map((item) => {
                if(item.article === value){
                    x.push(item)
                }
            })
        });
        setBuild(x);
    },[products])

    return (
        <div className='App'>
            <div className='product-list'>
                <CartItemsList build={build}
                               cart={cart}/>
            </div>
        </div>
    )
}
export default CartContainer;