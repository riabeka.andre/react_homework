import React, {useState} from "react";

function CartItem (props){
    const [open] = useState('disactive')
    const [isOpen, setIsOpen] = useState(false)
    const [hidden, setIsHidden] = useState(false)

    function modal  ()  {
        setIsOpen(
           true
        )
    }
    function removeFromCart  ()  {
        props.cart.find((value, index) => {
            if(value === props.article){
                props.cart.splice(index, 1);
                localStorage.setItem("cart", JSON.stringify(props.cart));
            }
        });

        setIsHidden(true)
        setIsOpen(false)
    }
    function doNotRemove  ()  {
        setIsOpen(false)
    }

        const body = isOpen && <div className='shadow-for-modal'>
            <div className='modal' >Are You sure You want to remove from cart this item?
                <button className='modalButton' onClick={removeFromCart}>Yes, remove</button>
                <button className='modalButton' onClick={doNotRemove}>No, don't add</button>
            </div>
        </div>;
        return(
                <>
                    {body}
                <div className={hidden ? "hidden" : "item"}><div className='delete' onClick={modal}>X</div>
                    <div className="items name">{props.name}</div>
                    <div className="items price">Price: <br/> {props.price} UAH</div>
                    <div className="items article">Article: {props.article}</div>
                    <div className="items color">Color: {props.color}</div>
                    <img className='img' src={props.src} alt="" />
                    {/*<div className='favorite'>*/}
                    {/*    <button className={open}></button>*/}
                    {/*</div>*/}
                </div>
                </>


        )

}
export default CartItem;