import React from "react";
import Product from "./Product"

 function ProductList (props) {
         return (
             props.products.map((item) => {
                 return (
                             <Product key={item.article}
                                      name={item.name}
                                      price={item.price}
                                      article={item.article}
                                      color={item.color}
                                      src={item.url}
                                      modal={props.modal}
                                      favorites={props.favorites}
                                      cart={props.cart}
                             countCarts={props.countCarts}/>
                     )
             })
         )
 }
 export default ProductList