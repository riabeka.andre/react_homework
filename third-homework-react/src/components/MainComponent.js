import React, { useState, useEffect } from 'react';
import ProductsList from "./ProductList";
import ProductService from "../services/Tasks";

export default function MainComponent () {
    const [products, setProducts] = useState([]);
    const [favorites, setFavorites] = useState([]);
    const [cart, setCart] = useState([]);
    const [number, setNumber] = useState(0);

     useEffect ( ( )  => {
         (async function () {
             let products = await ProductService.getProducts();
             setProducts(
                 products
             );
         })();
        const carts = JSON.parse(localStorage.getItem('cart'));
        if(carts !== null){
            setCart( carts );
        }
        if(carts === null){
            setCart(   [] );
            localStorage.setItem("cart", JSON.stringify(cart));
        }
        const favorite = JSON.parse(localStorage.getItem("favorites"));
        if(favorite !== null){
            setFavorites(favorite );
        }
        if(favorite === null){
            setFavorites([] );
            localStorage.setItem("favorites", JSON.stringify(favorites));
        }
    }, [], );

    function countCarts() {
        const cart = JSON.parse(localStorage.getItem('cart'));
        if(cart !== null){
            setCart( cart );
        }
        if(cart === null ){
            setCart([]);
        }
    };

    let itemsInCart = <div className='cartNumber'>Cart {cart.length}</div>;

    return (
        <div className='App'>
            {itemsInCart}
            <div className='product-list'>
                <ProductsList products={products}
                              favorites={favorites}
                              countCarts={countCarts}
                              cart={cart}/>
            </div>
        </div>
    )
}