import React, { useState, useEffect } from "react";

function Product (props) {
    const [open, setOpen] = useState('disactive');
    const [isOpen, setIsOpen] = useState(false);

   function changeClass  ()  {
        if(open === 'disactive'){
            setOpen('active');
            props.favorites.push(props.article);
            localStorage.setItem("favorites", JSON.stringify(props.favorites));
        }else{
            setOpen('disactive');
            props.favorites.find((value, index) => {
                if(value === props.article){
                    props.favorites.splice(index, 1);
                    localStorage.setItem("favorites", JSON.stringify(props.favorites));
                }
            });
        }
    }
    function modal ()  {
        setIsOpen(true)
    }
    function addToCart ()  {
        setIsOpen(false);
        props.cart.push(props.article);
        let {cart} = props;
        localStorage.setItem("cart", JSON.stringify(cart))
    }
   function doNotAdd () {
       setIsOpen(false);
    }
    useEffect(()  => {
        const liked = JSON.parse(localStorage.getItem("favorites"));
        if(liked !== null){
            liked.find((value,index) => {
                if(value === props.article){
                    setOpen('active');
                }
            })
        }

    });

        const body = isOpen && <div className='shadow-for-modal'>
            <div className='modal' >Are You sure You want to add to cart this item?
                <button className='modalButton'
                        onClick={ (e) => {addToCart(e);
                            props.countCarts(e)}}>Yes, add to cart</button>
                <button className='modalButton' onClick={doNotAdd}>No, don't add</button>
            </div>
        </div>;
        return (
            <>
                {body}
                <div className="item">
                    <div className="items name">{props.name}</div>
                    <div className="items price">Price: <br/> {props.price} UAH</div>
                    <div className="items article">Article: {props.article}</div>
                    <div className="items color">Color: {props.color}</div>
                    <img className='img' src={props.src} alt="" />
                    <button className='add-to-carte' onClick={modal}>Add to cart</button>
                    <div className='favorite'>
                        <button className={open} onClick={changeClass}></button>
                    </div>
                </div>
            </>

        )
    }



export default Product;