import React, {useState} from "react";

function Liked (props) {
    const [open, setOpen] = useState('active');
    const [hidden, setHidden] = useState(false);

    function changeClass  ()  {

            props.favorites.find((value, index) => {
                if(value === props.article){
                    props.favorites.splice(index, 1);
                    localStorage.setItem("favorites", JSON.stringify(props.favorites));
                }
            });
        setOpen('disactive');
        setHidden(true)
    }

        return(
            <>
            <div className={hidden ? "hidden" : "item"}>
                <div className="items name">{props.name}</div>
                <div className="items price">Price: <br/> {props.price} UAH</div>
                <div className="items article">Article: {props.article}</div>
                <div className="items color">Color: {props.color}</div>
                <img className='img' src={props.src} alt="" />
                {/*<button className='add-to-carte' onClick={props.modal}>Add to cart</button>*/}
                <div className='favorite'>
                    <button className={open} onClick={changeClass}></button>
                </div>
            </div>
                </>
        )

}
export default Liked;