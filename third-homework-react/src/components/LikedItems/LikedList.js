import React from "react";
import Liked from "./Liked"

function LikedList (props) {
        return (
            props.build.map((item) => {
                return (
                    <Liked key={item.article}
                             name={item.name}
                             price={item.price}
                             article={item.article}
                             color={item.color}
                             src={item.url}
                             modal={props.modal}
                             favorites={props.favorites}/>
                )
            })
        )
}
export default LikedList;
