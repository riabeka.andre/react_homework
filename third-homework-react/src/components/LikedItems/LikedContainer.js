import React, {useState, useEffect} from "react";
import ProductService from "../../services/Tasks";
import LikedList from "./LikedList";

function LikedContainer (){
    const [products, setProducts] = useState([]);
    const [favorites, setFavorites] = useState([]);
    const [build, setBuild] = useState([]);

    useEffect ( ( )  => {
        (async function () {
            let products = await ProductService.getProducts();
            setProducts(
                products
            );
        })() }, []);
    useEffect(() => {
        let x = [];
        let favorite = JSON.parse(localStorage.getItem("favorites"));
        if(favorite === null){
            setFavorites([] );
            localStorage.setItem("favorites", JSON.stringify(favorites));
        }
        if(favorite !== null){
            setFavorites(favorite );
        }
        favorites.map((value) => {
            products.map((item) => {
                if(item.article === value){
                    x.push(item)
                }
            })
        });
        setBuild(
            x
        );
    },  [products]);

        return (
            <div className='App'>
                <div className='product-list'>
                    <LikedList build={build}
                               favorites={favorites}/>
                </div>
            </div>



        )


}
export default LikedContainer;