import React from "react";
import {Link} from "react-router-dom"
class Nav extends React.Component{

    render() {
        return(
            <nav className='nav'>
                <ul className='nav-links'>
                    <Link to='/likedContainer'>
                        <li>Favorites</li>
                    </Link>
                    <Link to='/CartContainer'>
                        <li>Cart</li>
                    </Link>
                    <Link to='/'>
                        <li>Main Page</li>
                    </Link>
                </ul>
            </nav>
        )
    }

}
export default Nav;